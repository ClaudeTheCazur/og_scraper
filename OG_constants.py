"""Hold constant/default values needed througout service."""

OG_meta_tag_name_set = set(
  [
    'og:title',
    'og:site_name',
    'og:url',
    'og:title',
    'og:description',

    'og:image',
    'og:image:url',
    'og:image:secure_url',
    'og:image:type',
    'og:image:width',
    'og:image:height',

    'og:video'

    'og:type',
    'og:locale',

    'article:author:',
    'article:content_tier',
    'article:expiration_time',
    'article:modified_time',
    'article:published_time',
    'article:publisher',
    'article:section',
    'article:tag'
  ])

og_prefix_len = len(['og:'])

OG_meta_tag_fallback_values = {
        'og:title': {
            'tag_names': ['title'],
            },

        'og:description': {
            'tag_names': ['meta'],
            'tag_attr': {'name': "/[D,d]escription"},
            'field': 'content'
            },

        'og:image': {
            'tag_names': ['img'],
            },

        'og:image:width': {
            'tag_names': ['img'],
            'field': 'style[width]'
            },

        'og:image:height': {
            'tag_names': ['img'],
            'field': 'style[height]'
            },

        'og:image:url': {
            'tag_names': ['img'],
            'field': 'src'
            },

        'article:author': {
            'tag_names': ['address'],
            'tag_attr': {'class': "[A,a]uthor"}
            }

}
