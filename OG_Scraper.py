from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
from pprint import pprint
import urllib.request as url_req
from OG_urlparser import parse_URL
from OG_constants import OG_meta_tag_name_set as meta_set, og_prefix_len
from OG_constants import OG_meta_tag_fallback_values as fallback_values
from OG_cache import get_data_at_url, cache_retrieved_data, default_cache_time


def retrieve_og_data(raw_url):
    """
    Serve as main entry point for logic flow

    Keywords:
    @param str raw_url: unformatted url decoded
    """
    url = parse_URL(raw_url)
    print(url)
    og_cache_data, canon_url_map = get_data_at_url(url)
    if og_cache_data:
        return og_cache_data
    else:
        og_value_map, canon_url = open_comm_channel(raw_url)
        cache_retrieved_data(
                    og_value_map, url, canon_url,
                    canon_url_map, default_cache_time)

        return og_value_map


def open_comm_channel(url):
    """
    Make initial contact with external resource

    Keywords:
    @param str url: unformatted decoded raw url
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_valid_resp(resp):
                og_value_map, canon_url = begin_transaction_proc(url)
                return og_value_map, canon_url
            else:
                raise ValueError('Response at URL is not valid')
    except RequestException as e:
        print('Error during requests to {} to {}'
              .format(url, str(e)))


def begin_transaction_proc(url):
    """
    Serve as second point in logic flow. Sets up scraper
    environment and returns full maps of values needed.

    Keywords:
    @param str url: unformatted decoded raw url
    """
    scraper_env, urlopen_object = setup_scraper_env(url)
    unfound_tag_set, partial_value_map = check_if_OG_present(url, scraper_env)
    print(unfound_tag_set)

    fallback_partial_value_map = query_unfound_metatags(
            unfound_tag_set, urlopen_object, fallback_values)

    total_value_map = {
        **partial_value_map,
        **fallback_partial_value_map
        }

    return total_value_map, scraper_env.get('canon_url')


def setup_scraper_env(url):
    """
    Set up environment object where most initial data values
    will be stored such as the entirety of 'urlopen_object'.

    Keywords:
    @param str url: unformatted decoded raw url
    """
    scraper_env = {'url': url}
    urlopen_object = setup_urlobject(url)

    unfound_tag_set = set(list(meta_set))
    scraper_env.update({'unfound_tag_set': unfound_tag_set})

    og_metadata_tag_value_map = {}
    scraper_env.update({
                      'og_metadata_tag_value_map':
                       og_metadata_tag_value_map
                       })


    canonical_url = retrieve_canonical_url(urlopen_object)
    scraper_env.update({'canonical': canonical_url})

    meta_tags = collect_meta_tags(urlopen_object)
    scraper_env.update({'meta_tags': meta_tags})

    return scraper_env, urlopen_object


def setup_urlobject(url):
    """
    Setup urlobject that will consist of resource parsed
    with html5lib and bs4 methods to access entities

    Keywords:
    @param str url: unformatted decoded raw url
    """
    req = url_req.Request(url, headers={
                        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64)'
                        'AppleWebkit/573.36 (KHTML, like Gecko)'
                        'Chrome/64.0.3282.167 Safari/573.36'})
    urlopen_object = BeautifulSoup(
        url_req.urlopen(req).read(), 'html5lib')

    return urlopen_object


def collect_meta_tags(urlopen_object):
    meta_tags = urlopen_object.find_all('meta')
    return meta_tags

def retrieve_canonical_url(urlopen_object):
    canonical_url = urlopen_object.find('link', ref='canonical')
    return canonical_url


def check_if_OG_present(url, scraper_env):
    """
    Look for OG values initially;
    first passthrough of parsed resource.

    Keywords:
    @param str url: unformatted decoded raw url
    @param map<str, array|set| map<>> scraper_env: environment object
    """

    meta_tags = scraper_env.get('meta_tags')
    og_metadata_tag_value_map = scraper_env.get('og_metadata_tag_value_map')
    unfound_tag_set = scraper_env.get('unfound_tag_set')

    for tag in meta_tags:
        og_property = tag.get('property', None)
        if og_property in meta_set:
            if (tag.get('content', None)):
                og_metadata_tag_value_map.update(
                    {og_property: tag.get('content')})

                unfound_tag_set = remove_metatag(unfound_tag_set, og_property)

    return unfound_tag_set, og_metadata_tag_value_map


def is_valid_resp(response):
    """
    Check if response returned from resource is valid.

    Keyword
    @param request_object response: response from contacting resource
    """

    content_type = response.headers['Content-Type'].lower()
    healthy_response = (response.status_code == 200)
    present_content = (content_type is not None)
    nonempty_html = (content_type.find('html') > 1)
    return (healthy_response
            and present_content
            and nonempty_html)


def remove_metatag(unfound_tag_set, found_tag):
    """
    Remove a tag once found

    Keywords:
    @param set([str]) unfound_tag_set: set of tags not found
    @param str found_tag: tag that needs to be removed
    """

    unfound_tag_set.difference(set(found_tag))
    return unfound_tag_set


def query_unfound_metatags(unfound_tag_set, urlopen_object, fallback_values):
    """
    Second passthrough using fallback values
    for unmatched/unfound tags

    Keywords:
    @param set([str]) unfound_tag_set: set of tags remaining
    @param bs4_object urlopen_object: contains parsed resource
    @param map<str, map<str,list>> fallback_values: fallback_values
    """

    fallback_partial_value_map = {}
    unfound_tag_list = list(unfound_tag_set)
    for tag in unfound_tag_list:
        print(tag)
        if tag in fallback_values:
            fallback_list = fallback_values[tag]
            result = query_fallback(fallback_list, urlopen_object)
            if result:
                print(result)
                fallback_partial_value_map.update({tag: result})
        else:
            pass

    return fallback_partial_value_map


def query_fallback(fallback_list, urlopen_object):
    """
    Individual query for certain tag

    Keywords:
    @param map<str, list> fallback_list: contains fallback tag names/attrs
    @param bs4bs4_object urlopen_object: contains parsed resource
    """

    print('inside query_fallback')
    tag_names = fallback_list.get('tag_names', [])
    tag_attr = fallback_list.get('tag_attr', '')

    for tag_name in tag_names:
        matched_tag = urlopen_object.find(tag_name, attrs=tag_attr)
        if matched_tag:
            tag_field = fallback_list.get('field', None)
            if tag_field:
                return matched_tag.get(tag_field, '')
            else:
                return matched_tag.text

    return None
