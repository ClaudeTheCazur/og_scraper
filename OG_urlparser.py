from urllib.parse import urlparse


def parse_URL(url):
    parsed_URL_object = urlparse(url)
    scheme = parsed_URL_object.scheme
    host_name = parsed_URL_object.hostname
    path = parsed_URL_object.path
    parsed_URL = '{}://{}{}'.format(scheme, host_name, path)
    return parsed_URL
