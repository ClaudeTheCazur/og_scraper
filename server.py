from flask import Flask, request, jsonify
from flask_restful import Resource, Api
from OG_Scraper import retrieve_og_data
from urllib.parse import unquote


app = Flask(__name__)
api = Api(app)


class Retrieved_OG_DATA(Resource):
    """
    Main resource endpoint which serves as an interface for outside world.

    Keywords:
    @param <flask-provided> Resource: flask provided resource object
    """

    def get(self, url_param):
        """
        Get method for this resource endpoint.

        Keywords:
        @param str url_param: encoded url in question
        """
        app.logger.debug(url_param)
        decoded_url = 'https://{}'.format(unquote(url_param))
        app.logger.debug(decoded_url)
        url_og_data = retrieve_og_data(decoded_url)
        results = {'data': url_og_data}
        return results


api.add_resource(Retrieved_OG_DATA, '/ogscraper/<string:url_param>')

if __name__ == '__main__':
    app.run(port=3000)
