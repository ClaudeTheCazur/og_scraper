import redis
import re

redisClient = redis.StrictRedis(host='localhost', port=6379, db=0)
default_cache_time = 3600


def get_data_at_url(url):
    """
    Return cached_data or empty dict from Redis.

    Keyword arguments:
    @param str url: non-canonical url
    """
    if redisClient.exists('url_canonURL_map'):
        url_canonURL_map = redisClient.hgetall('url_canonURL_map')
        if url in url_canonURL_map:
            canon_url = url_canonURL_map[url]
            og_cached_data = redisClient.get(canon_url)
            return og_cached_data, {}
    else:
        url_canonURL_map = {}
    return None, url_canonURL_map


def cache_retrieved_data(og_value_map, url, canon_url,
                         url_canonURL_map, default_cache_time):
    """
    Cache the values map retrieved from OG_Scraper

    Keyword arguments:
    @param dict og_value_map: map<og_tags,values>
    @param str url: formatted url
    @param str canon_url: canonical url
    @param dict url_canonURL_map: map<formatted_url, canonical_url>
    @param int default_cache_time: default time for cache store

    """
    url_canonURL_map.update({url: canon_url})
    redisClient.setex(canon_url, default_cache_time, og_value_map)
    redisClient.hmset('url_canonURL_map', url_canonURL_map)
    return
